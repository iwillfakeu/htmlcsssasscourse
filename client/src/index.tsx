import React from "react";
import ReactDOM from "react-dom";
import "./css/icon-font.css";
import "./css/index.css";
import App from "./App";

ReactDOM.render(<App />, document.getElementById("root"));
