import styled, { CSSProp } from "styled-components";

import { colors, fontSizes } from "../../layout/constants";

export const Section = styled.section`
  background-color: ${colors.greyLight1};
  padding: 25rem 0;
  margin-top: -20vh;
`;

export const TitleContainer = styled.div<{ marginBottom?: CSSProp }>`
  text-align: center;
  margin-bottom: ${props => props.marginBottom || "0"};
`;

export const Title = styled.h2`
  font-size: 3.5rem;
  text-transform: uppercase;
  font-weight: 700;
  display: inline-block;
  background-image: linear-gradient(
    to right,
    ${colors.primaryLight},
    ${colors.primaryDark}
  );
  background-clip: text;
  -webkit-background-clip: text;
  color: transparent;
  letter-spacing: 0.2rem;
  transition: all 0.2s;

  &:hover {
    transform: skewY(2deg) skewX(15deg) scale(1.1);
    text-shadow: 0.5rem 1rem 2rem rgba(0, 0, 0, 0.2);
  }
`;

export const Subtitle = styled.h3<{ marginBottom?: CSSProp }>`
  font-size: ${fontSizes.defaultFontSize};
  font-weight: 700;
  text-transform: uppercase;
  margin-bottom: ${props => props.marginBottom || "0"};
`;

export const Paragraph = styled.p`
  font-size: ${fontSizes.defaultFontSize};

  &:not(:last-child) {
    margin-bottom: 3rem;
  }
`;

export const Button = styled.a`
  &:link,
  &:visited {
    color: ${colors.primary};
    display: inline-block;
    text-decoration: none;
    border-bottom: 1px solid ${colors.primary};
    padding: 3px;
    font-size: ${fontSizes.defaultFontSize};
    transition: all 0.2s;
  }

  &:hover {
    background-color: ${colors.primary};
    color: ${colors.white};
    box-shadow: 0 1rem 2rem rgba(0, 0, 0, 0.15);
    transform: translateY(-2px);
  }

  &:active {
    box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15);
    transform: translateY(0);
  }
`;
