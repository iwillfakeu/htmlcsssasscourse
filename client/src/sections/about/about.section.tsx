import React from "react";

//  styles
import {
  Section,
  TitleContainer,
  Title,
  Subtitle,
  Paragraph,
  Button
} from "./about.styles";

//  layout
import { Row, Col1of2 } from "../../layout/grid";

//  components
import ImagesViewer from "../../components/images-viewer/images-viewer.component";

const About: React.FC = (): JSX.Element => (
  <main>
    <Section>
      <TitleContainer marginBottom={"8rem"}>
        <Title>Exciting tours for adventurous people</Title>
      </TitleContainer>
      <Row>
        <Col1of2>
          <Subtitle marginBottom={"1.5rem"}>
            You're going to fall in love with nature
          </Subtitle>
          <Paragraph>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur
            omnis iste rerum eius id architecto facere. Saepe dolorem facere
            nisi eligendi tempora. Obcaecati placeat sed eaque voluptate atque
            amet adipisci!
          </Paragraph>
          <Subtitle marginBottom={"1.5rem"}>
            Live adventures like you never have before
          </Subtitle>
          <Paragraph>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque
            illum vel unde possimus delectus officiis deleniti, voluptatibus
            earum at? Unde veritatis necessitatibus nesciunt illo illum
            architecto sint. Nulla, ab accusamus!
          </Paragraph>
          <Button href="#">Learn more &rarr;</Button>
        </Col1of2>
        <Col1of2>
          <ImagesViewer />
        </Col1of2>
      </Row>
    </Section>
  </main>
);
export default About;
