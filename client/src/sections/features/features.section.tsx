import React from "react";

import { Row, Col1of4 } from "../../layout/grid";

import FeatureBox from "../../components/feature-box/feature-box.component";

import { Section } from "./features.styles";

const cards = [
  {
    title: "Explore the world",
    icon: "icon-basic-world",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur doloremque assumenda id, eligendi incidunt similique culpa quis numquam impedit quia quidem asperiores, minus dolorem cumque molestiae unde! Consectetur, nulla. Distinctio?"
  },
  {
    title: "Meet nature",
    icon: "icon-basic-compass",
    description:
      "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Amet expedita totam aliquid laboriosam culpa placeat eveniet quisquam soluta, necessitatibus accusantium aperiam, debitis eum, reiciendis veniam. Modi assumenda officiis nobis quasi."
  },
  {
    title: "Find your way",
    icon: "icon-basic-map",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate quia eos aut quaerat. Nulla labore illum odio modi dicta perspiciatis sunt aut repellat est impedit, delectus accusamus voluptates eius facere."
  },
  {
    title: "Live a healthier life",
    icon: "icon-basic-heart",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum a ipsum quidem quia optio quos perspiciatis repellendus eaque inventore ipsa placeat quo pariatur nisi cupiditate at vitae voluptatem, mollitia nihil?"
  }
];

const Features: React.FC = (): JSX.Element => (
  <Section>
    <Row>
      {cards.map((card, idx) => (
        <Col1of4 key={idx}>
          <FeatureBox
            title={card.title}
            description={card.description}
            icon={card.icon}
          />
        </Col1of4>
      ))}
    </Row>
  </Section>
);

export default Features;
