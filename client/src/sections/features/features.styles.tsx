import styled from "styled-components";
import background from "../../assets/nat-4.jpg";

export const Section = styled.section`
  padding: 20rem 0;
  background-image: linear-gradient(
      to right,
      rgba(126, 213, 111, 0.8),
      rgba(40, 180, 131, 0.8)
    ),
    url(${background});
  background-size: cover;
  transform: skewY(-7deg);
  margin-top: -12rem;

  & > * {
    transform: skewY(7deg);
  }
`;
