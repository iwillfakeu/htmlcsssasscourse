import styled from "styled-components";

import hero from "../../assets/hero.jpg";

export const Section = styled.header`
  height: 95vh;
  background-image: linear-gradient(
      to right,
      rgba(126, 213, 111, 0.8),
      rgba(40, 180, 131, 0.8)
    ),
    url(${hero});
  background-size: cover;
  background-position: top;
  clip-path: polygon(0 0, 100% 0, 100% 75vh, 0 100%);
  position: relative;
`;

export const CenterContainer = styled.div`
  position: absolute;
  top: 40%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
`;

export const LogoContainer = styled.div`
  position: absolute;
  top: 4rem;
  left: 4rem;
`;
