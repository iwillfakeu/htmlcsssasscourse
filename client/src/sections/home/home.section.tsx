import React from "react";

//  components
import LogoImage from "../../components/logo/logo.component";
import Slogan from "../../components/slogan/slogan.component";
import ButtonHome from "../../components/button-home/button-home.component";

//  styles
import { Section, CenterContainer, LogoContainer } from "./home.styles";

const Home: React.FC = (): JSX.Element => (
  <Section>
    <LogoContainer>
      <LogoImage />
    </LogoContainer>
    <CenterContainer>
      <Slogan />
      <ButtonHome />
    </CenterContainer>
  </Section>
);

export default Home;
