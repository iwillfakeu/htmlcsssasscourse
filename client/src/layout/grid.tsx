import styled from "styled-components";

import { size } from "./constants";

const ColMixin = () => `
  float: left;

  &:not(:last-child) {
    margin-right: ${size.gutterHorizontal};
  }
`;

export const Row = styled.div`
  max-width: ${size.gridWidth};
  margin: 0 auto;

  &:not(:last-child) {
    margin-bottom: ${size.gutterVertical};
  }

  &::after {
    content: "";
    display: table;
    clear: both;
  }
`;

export const Col1of2 = styled.div`
  width: calc((100% - ${size.gutterHorizontal}) / 2);
  ${ColMixin}
`;

export const Col1of3 = styled.div`
  width: calc((100% - (2 * ${size.gutterHorizontal})) / 3);
  ${ColMixin}
`;

export const Col2of3 = styled.div`
  width: calc(
    2 * ((100% - (2 * ${size.gutterHorizontal})) / 3) + ${size.gutterHorizontal}
  );
  ${ColMixin}
`;

export const Col1of4 = styled.div`
  width: calc((100% - (3 * ${size.gutterHorizontal})) / 4);
  ${ColMixin}
`;

export const Col2of4 = styled.div`
  width: calc(
    2 * ((100% - (3 * ${size.gutterHorizontal})) / 4) + ${size.gutterHorizontal}
  );
  ${ColMixin}
`;

export const Col3of4 = styled.div`
  width: calc(
    3 * ((100% - (3 * ${size.gutterHorizontal})) / 4) +
      (2 * ${size.gutterHorizontal})
  );
  ${ColMixin}
`;
