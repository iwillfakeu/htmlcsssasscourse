export const size = {
  gridWidth: "114rem",
  gutterVertical: "8rem",
  gutterHorizontal: "6rem"
};

export const fontSizes = {
  defaultFontSize: "1.6rem"
};

export const colors = {
  primary: "#55C57A",
  primaryLight: "#7ed56f",
  primaryDark: "#28b485",

  greyDark: "#777",
  greyLight1: "#f7f7f7",
  white: "#fff",
  black: "#000"
};
