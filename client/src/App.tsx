import React from "react";

import HomePage from "./pages/home/home.page";

const App: React.FC = (): JSX.Element => <HomePage />;
export default App;
