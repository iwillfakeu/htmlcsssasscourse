import React, { Fragment } from "react";

//  sections
import Home from "../../sections/home/home.section";
import About from "../../sections/about/about.section";
import Features from "../../sections/features/features.section";

const HomePage: React.FC = (): JSX.Element => (
  <Fragment>
    <Home />
    <About />
    <Features />
  </Fragment>
);

export default HomePage;
