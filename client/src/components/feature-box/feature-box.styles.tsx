import styled, { CSSProp } from "styled-components";
import { fontSizes, colors } from "../../layout/constants";

export const Container = styled.div`
  background-color: rgba(255, 255, 255, 0.8);
  font-size: 1.5rem;
  padding: 2.5rem;
  text-align: center;
  border-radius: 3px;
  box-shadow: 0 1.5rem 4rem rgba(0, 0, 0, 0.15);
  transition: all 0.3s;

  &:hover {
    transform: translateY(-1.5rem) scale(1.03);
  }
`;

export const Icon = styled.i`
  font-size: 6rem;
  margin-bottom: 0.5rem;
  display: inline-block;
  background-image: linear-gradient(
    to right,
    ${colors.primaryLight},
    ${colors.primaryDark}
  );
  background-clip: text;
  -webkit-background-clip: text;
  color: transparent;
`;

export const Subtitle = styled.h3<{ marginBottom?: CSSProp }>`
  font-size: ${fontSizes.defaultFontSize};
  font-weight: 700;
  text-transform: uppercase;
  margin-bottom: ${props => props.marginBottom || "0"};
`;
