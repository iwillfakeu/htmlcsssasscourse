import React from "react";

import { Subtitle, Container, Icon } from "./feature-box.styles";

interface FeatureBoxProps {
  description?: string;
  title?: string;
  icon?: string;
}

const FeatureBox: React.FC<FeatureBoxProps> = ({
  description,
  title,
  icon
}): JSX.Element => (
  <Container>
    <Icon className={icon} />
    <Subtitle marginBottom={"1.5rem"}>{title}</Subtitle>
    <p>{description}</p>
  </Container>
);

export default FeatureBox;
