import styled from "styled-components";

export const Container = styled.div`
  perspective: 150rem;
  position: relative;
  height: 50rem;
`;

export const SideFront = styled.div`
  background-color: orangered;
  color: #fff;
  font-size: 2rem;

  height: 50rem;
  transition: all 0.8s;

  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  backface-visibility: hidden;

  &:hover {
    transform: rotateY(180deg);
  }
`;

export const SideBack = styled.div`
  background-color: green;
  color: #fff;
  font-size: 2rem;

  height: 50rem;
  transition: all 0.8s;

  position: absolute;
  top: 0;
  left: 0;
  width: 100%;

  backface-visibility: hidden;

  transform: rotateY(180deg);

  &:hover {
    transform: rotateY(0);
  }
`;

// export const Side = styled.div<{ isFront?: boolean }>`
//   background-color: ${props => (props.isFront ? "orangered" : "green")};
//   color: #fff;
//   font-size: 2rem;
//   height: 50rem;
//   transition: all 0.8s;
//   transform: ${props => (props.isFront ? "" : "rotateY(180deg)")};
//   position: absolute;
//   top: 0;
//   left: 0;
//   width: 100%;

//   &:hover {
//     transform: ${props => (props.isFront ? "rotateY(180deg)" : "rotateY(0)")};
//   }
// `;
