import React from "react";

//  styles
import { Container, SideFront, SideBack } from "./card.styles";

const Card: React.FC = (): JSX.Element => (
  <Container>
    <SideFront>Front</SideFront>
    <SideBack>Back</SideBack>
  </Container>
);

export default Card;
