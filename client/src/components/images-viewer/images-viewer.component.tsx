import React from "react";

import image1 from "../../assets/nat-1-large.jpg";
import image2 from "../../assets/nat-2-large.jpg";
import image3 from "../../assets/nat-3-large.jpg";

import { ImagesContainer, Image } from "./images-viewer.styles";

const ImagesViewer: React.FC = (): JSX.Element => (
  <ImagesContainer>
    <Image src={image1} alt="1" />
    <Image src={image2} alt="2" />
    <Image src={image3} alt="3" />
  </ImagesContainer>
);

export default ImagesViewer;
