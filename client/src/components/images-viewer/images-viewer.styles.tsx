import styled from "styled-components";

import { colors } from "../../layout/constants";

export const ImagesContainer = styled.div`
  position: relative;
  transition: all 0.2s;

  &:hover {
    transform: scale(0.95);
  }
`;

export const Image = styled.img`
  width: 55%;
  box-shadow: 0 1.5rem 4rem rgba(0, 0, 0, 0.4);
  border-radius: 2px;
  position: absolute;
  z-index: 10;
  transition: all 0.2s;
  outline-offset: 2rem;

  &:first-child {
    left: 0;
    top: -2rem;
  }

  &:nth-child(2) {
    right: 0;
    top: 2rem;
  }

  &:last-child {
    left: 20%;
    top: 10rem;
  }

  &:hover {
    outline: 1.5rem solid ${colors.primary};
    transform: scale(1.2) translateY(-0.5rem);
    box-shadow: 0 2.5rem 4rem rgba(0, 0, 0, 0.5);
    z-index: 20;
  }
`;
