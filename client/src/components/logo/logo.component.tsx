import React from "react";

import { Logo } from "./logo.styles";

import logo from "../../assets/logo-white.png";

const LogoImage: React.FC = (): JSX.Element => <Logo src={logo} alt="Logo" />;

export default LogoImage;
