import styled, { keyframes } from "styled-components";

import { colors, fontSizes } from "../../layout/constants";

const moveInBottom = keyframes`
  0%{
    opacity: 0;
    transform: translateY(3rem);
  }
  100%{
    opacity: 1;
    transform: translateY(0);
  }
`;

export const Button = styled.a`
  &:link,
  &:visited {
    text-transform: uppercase;
    text-decoration: none;
    color: ${colors.greyDark};
    background-color: ${colors.white};
    padding: 1.5rem 4rem;
    display: inline-block;
    border-radius: 10rem;
    transition: all 0.2s;
    font-size: ${fontSizes.defaultFontSize};
    position: relative;
    animation: ${moveInBottom} 0.5s ease-out 0.75s;
    animation-fill-mode: backwards;

    &:hover {
      transform: translateY(-0.3rem);
      box-shadow: 0 1rem 2rem rgba(0, 0, 0, 0.2);

      &::after {
        transform: scaleX(1.4) scaleY(1.6);
        opacity: 0;
      }
    }

    &:active {
      transform: translateY(-0.1rem);
      box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.2);
    }

    &::after {
      content: "";
      display: inline-block;
      height: 100%;
      width: 100%;
      border-radius: 10rem;
      background-color: ${colors.white};
      position: absolute;
      top: 0;
      left: 0;
      z-index: -1;
      transition: all 0.4s;
    }
  }
`;
