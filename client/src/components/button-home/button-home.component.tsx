import React from "react";

import { Button } from "./button-home.styles";

const ButtonHome: React.FC = (): JSX.Element => (
  <Button href="#">Discover our tours</Button>
);

export default ButtonHome;
