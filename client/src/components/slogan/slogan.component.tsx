import React from "react";

import { MainText, PrimaryMainText, SecondaryMainText } from "./slogan.styles";

const Slogan: React.FC = (): JSX.Element => (
  <MainText>
    <PrimaryMainText>OUTDOORS</PrimaryMainText>
    <SecondaryMainText>is where life happens</SecondaryMainText>
  </MainText>
);

export default Slogan;
