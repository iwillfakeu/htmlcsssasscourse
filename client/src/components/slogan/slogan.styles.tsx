import styled, { keyframes } from "styled-components";

import { colors } from "../../layout/constants";

const moveInLeft = keyframes`
0%{
  opacity: 0;
  transform: translateX(-10rem);
}
80%{
  transform: translateX(1rem);
}
100%{
  opacity: 1;
  transform: translateX(0);
}
`;

const moveInRight = keyframes`
0%{
  opacity: 0;
  transform: translateX(10rem);
}
80%{
  transform: translateX(-1rem);
}
100%{
  opacity: 1;
  transform: translateX(0);
}
`;

export const MainText = styled.h1`
  color: ${colors.white};
  text-transform: uppercase;

  backface-visibility: hidden;
  margin-bottom: 6rem;
`;

export const PrimaryMainText = styled.span`
  display: block;
  font-size: 6rem;
  font-weight: 400;
  letter-spacing: 3.6rem;

  animation: ${moveInLeft} 1s ease-in;
`;

export const SecondaryMainText = styled.span`
  display: block;
  font-size: 2rem;
  font-weight: 700;
  letter-spacing: 1.75rem;
  animation: ${moveInRight} 1s ease-in;
`;
